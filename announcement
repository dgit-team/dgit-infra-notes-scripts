I am pleased to announce dgit 1.0, which can be used, as applicable,
by all contributors and downstreams.

dgit allows you to treat the Debian archive as if it were a git
repository, and get a git view of any package.  If you have the
appropriate access rights you can do builds and uploads from git, and
other dgit users will see your git history.

(dgit's git histories are often disjoint from the maintainer's
history; this is because the dgit git tree has exactly the same
content as the archive, which maintainers' histories often don't.)


Obtaining dgit
--------------

dgit 1.0 was just uploaded to unstable.

The source (latest found in sid) can be obtained via
  dgit clone dgit

The last dgitish upload of dgit itself can be found here
  w3m https://browse.dgit.debian.org/dgit.git/
  git clone https://git.dgit.debian.org/dgit
(but note that this would not reflect any NMUs not done with dgit.)


Existing users
--------------

All existing users who are not running dgit 0.30 or later must
upgrade.  dgit << 0.30 will not cope with git histories manipulated by
later dgit, and will crash trying to access now-moved-aside git
repositories on alioth.  New versions of dgit accept old histories
(and fix them up if necessary).


Anonymous read-only access
--------------------------

dgit 1.0 can be used by anyone to get the source for packages in the
Debian archive.  Changes can then be made in git, and binary packages
built using your favourite build tools.  You can share changes made in
git in the normal gitish ways.


Debian maintainers
------------------

At the moment, the Debian Project lacks a database listing the public
ssh keys of DMs.  The dgit push infrastructure requires the user's ssh
key to be installed.

For now, this will be done manually.  If you are a DM and would like
to use `dgit push', please send a GPG-signed copy of your ssh public
key to dgit-owner@debian.org.


Backports
---------

The dgit 1.0 binary package which I have just uploaded to sid is
installable (simply with dpkg -i), and useable back to at least
wheezy.

The source builds on wheezy as-is.  I hope that a formal backport will
be forthcoming soon.


Exciting uses of some of the dgit-repos on alioth
-------------------------------------------------

The previous location for the dgit git repositories was on alioth, in
/git/dgit-repos/repos, where they were writeable by the scm_collab-qa
and Debian groups.  It appears that some contributors took advantage
of this (deliberately or perhaps accidentally) to store various
ancillary git branches and tags in the dgit git repos.

These extra branches and tags have not been reproduced in the new dgit
git service.  (I intend for the dgit git service to become a general
purpose git server with access control similar to that of the Debian
archive, but for now it is not possible to manipulate the refs in the
dgit git service other than via dgit push.)

To prevent old dgits from accidentally operating on stale history, I
have moved these old repos on alioth aside, to
/git/dgit-repos/repos.now-on-gideon.

If you were using these as general-purpose git remotes, please copy
(or move) the data elsewhere.


Thanks
------

I would like to thank DSA and ftpmaster.

ftpmaster have provided the new archive data API service, which
supports the anonymous queries needed for readonly use of dgit.

DSA have provided hosting for both the main dgit git service, and the
mirror used for read access.  The support I have had from the DSA
team, and particularly Peter Palfrader, has been both excellent and
invaluable.  Without their help none of this would have been possible.
